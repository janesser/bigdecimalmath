# BigDecimal Arithmetics for Scala (BigDecimalMath4Scala)
Be as precise as you like (or as much as you can afford in terms of runtime).
BigDecimal precision is maintained throughout calculations. That means function parameter and result have equal scale. ZERO is a special case anyway.

Actually [scala.math.BigDecimal](http://www.scala-lang.org/api/2.10.4/index.html#scala.math.BigDecimal) and alike are almost equal (or worse) than their Java correspondants [java.math.BigDecimal](http://docs.oracle.com/javase/7/docs/api/java/math/BigDecimal.html). 
Having some functional structures at hand may improve the way to think about computating formulas.

Intention of this extending library is:

* to make use of BigDecimal easier
* to make code more fluent to read
* to give control over the scale and precision of calculation
* to provide a rich feature set of numeric computations (which match the every requested precision).

Project has moved [here](https://github.com/janesser/BigDecimalMath4Scala).
